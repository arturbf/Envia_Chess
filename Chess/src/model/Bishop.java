package model;

import java.awt.Point;

import control.SquareControl;



public class Bishop extends Piece {

	public Bishop() {
		// TODO Auto-generated constructor stub
	}

	public Bishop(String imagePath) {
		super(imagePath);
		if(imagePath.compareTo("icon/Yellow B_48x48.png")==0){
			setColor("Yellow");
		}
		else{
			setColor("Black");
		}
	}

	@Override
	public void unShowMove(SquareControl squareControl) {
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		for(int i = point.x, j = point.y;( i <=7 && j <=7) ; i++, j++){
			square = squareControl.getSquare(i,j);	
			squareControl.resetColor(square);	
		}
		for(int i = point.x-1, j = point.y+1; (i >=0 && j <=7) ; i--, j++){
			square = squareControl.getSquare(i,j);	
			squareControl.resetColor(square);		
		}
		for(int i = point.x-1, j = point.y-1;( i>=0 && j>=0) ; i--, j--){
			square = squareControl.getSquare(i,j);	
			squareControl.resetColor(square);		
		}
		for(int i = point.x+1, j = point.y-1;( i<=7 && j>=0) ; i++, j--){
			square = squareControl.getSquare(i,j);	
			squareControl.resetColor(square);		
		}
	}

	@Override
	public void showMove(SquareControl squareControl) {
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		for(int i = point.x+1, j = point.y+1;( i <=7 && j <=7) ; i++, j++){
			square = squareControl.getSquare(i,j);
			if(square.getPiece()!=null && square.getPiece().getColor() == squareControl.getSelectedSquare().getPiece().getColor() ){
				break;
			}
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor()){
				break;
			}
		}
		for(int i = point.x-1, j = point.y+1; (i >=0 && j <=7) ; i--, j++){
			square = squareControl.getSquare(i,j);
			if(square.getPiece()!=null && square.getPiece().getColor() == squareControl.getSelectedSquare().getPiece().getColor() ){
				break;
			}
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor()){
				break;
			}
		}
		for(int i = point.x-1, j = point.y-1;( i>=0 && j>=0) ; i--, j--){
			square = squareControl.getSquare(i,j);	
			if(square.getPiece()!=null && square.getPiece().getColor() == squareControl.getSelectedSquare().getPiece().getColor() ){
				break;
			}
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor()){
				break;
			}
		}
		for(int i = point.x+1, j = point.y-1;( i<=7 && j>=0) ; i++, j--){
			square = squareControl.getSquare(i,j);	
			if(square.getPiece()!=null && square.getPiece().getColor() == squareControl.getSelectedSquare().getPiece().getColor() ){
				break;
			}
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);		
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor()){
				break;
			}
		}
	}

	

}
