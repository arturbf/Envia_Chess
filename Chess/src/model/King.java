package model;

import java.awt.Point;

import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

import control.SquareControl;



public class King extends Piece {

	public King() {
		// TODO Auto-generated constructor stub
	}

	public King(String imagePath) {
		super(imagePath);
		if(imagePath.compareTo("icon/Yellow K_48x48.png")==0){
			setColor("Yellow");
		}
		else{
			setColor("Black");
		}
	}

	@Override
	public void unShowMove(SquareControl squareControl) {
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		//Half Rook
		square = squareControl.getSquare(point.x+1,point.y);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x-1,point.y);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x,point.y+1);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x,point.y-1);	
		squareControl.resetColor(square);
		//Half Bishop		
		square = squareControl.getSquare(point.x+1,point.y+1);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x-1,point.y-1);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x+1,point.y-1);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x-1,point.y+1);	
		squareControl.resetColor(square);
//		square = squareControl.getSquare(point.x,point.y+2);
//		squareControl.resetColor(square);
//		square = squareControl.getSquare(point.x,point.y-2);
//		squareControl.resetColor(square);
	}
//	public void showMoveCastling(SquareControl squareControl){
//		Point point = squareControl.getSelectedSquare().getPosition();
//		Square square;
//		try{
//		Rook rook = (Rook) square.getPiece();		
//	if(squareControl.getSelectedSquare().getPiece().getColor()== "Black"){
//			if(firstMove && rook.getFirstMove() && rook.getName()== "RookOfQueen" ){
//				square = squareControl.getSquare(point.x,point.y+2);
//				square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
//			}
//			if(firstMove && rook.getFirstMove() && rook.getName()== "RookOfKing" ){
//				square = squareControl.getSquare(point.x,point.y-2);
//				square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
//			}	
//		}
//	}catch(Exception e){}
//	}
	@Override
	public void showMove(SquareControl squareControl) {
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		//Half Rook
		square = squareControl.getSquare(point.x+1,point.y);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x-1,point.y);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x,point.y+1);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x,point.y-1);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		//Half Bishop
		
		square = squareControl.getSquare(point.x+1,point.y+1);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x-1,point.y-1);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);	
		square = squareControl.getSquare(point.x+1,point.y-1);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x-1,point.y+1);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
//		showMoveCastling(squareControl);
	}



}
