package model;

import java.awt.Point;

import control.SquareControl;



public class Knight extends Piece {

	public Knight() {
		// TODO Auto-generated constructor stub
	}

	public Knight(String imagePath) {
		super(imagePath);
		if(imagePath.compareTo("icon/Yellow N_48x48.png")==0){
			setColor("Yellow");
		}
		else{
			setColor("Black");
		}
	}

	@Override
	public void unShowMove(SquareControl squareControl) {
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		//Pra frente
		square = squareControl.getSquare(point.x+2,point.y+1);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x+2,point.y-1);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x-2,point.y+1);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x-2,point.y-1);	
		squareControl.resetColor(square);
//		Pros lados
		square = squareControl.getSquare(point.x+1,point.y+2);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x+1,point.y-2);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x-1,point.y+2);	
		squareControl.resetColor(square);
		square = squareControl.getSquare(point.x-1,point.y-2);	
		squareControl.resetColor(square);
		
	}

	@Override
	public void showMove(SquareControl squareControl) {
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		//Pra frente
		square = squareControl.getSquare(point.x+2,point.y+1);	
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x+2,point.y-1);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x-2,point.y+1);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x-2,point.y-1);	
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		//Pros lados
		square = squareControl.getSquare(point.x+1,point.y+2);	
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x+1,point.y-2);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x-1,point.y+2);	
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		square = squareControl.getSquare(point.x-1,point.y-2);
		if(square.getPiece()==null || square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);		
	}

	

}
