package model;

import java.awt.Point;

import control.SquareControl;


public class Rook extends Piece {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Rook(String imagePath) {
		super(imagePath);
		if(imagePath.compareTo("icon/Yellow R_48x48.png")==0){
			setColor("Yellow");
		}
		else{
			setColor("Black");
		}
	}
	public String name(Square square){
		Point point = square.getPosition();
		if(point.getX()==0 && (point.getY()==0 || point.getY() == 7)  ){
			return "RookOfKing";
		}
		else
			return "RookOfQueen";
	}

	
	public void showMove(SquareControl squareControl){
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		for (int i = point.y; i < SquareControl.COL_NUMBER; i++) {
			square = squareControl.getSquare(point.x,i+1);	
			if(square.getPiece()!=null && square.getPiece().getColor() == squareControl.getSelectedSquare().getPiece().getColor() ){
				break;
			}
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor()){
				break;
			}
		}
		for (int i = point.y; i >= 0; i--) {
			square = squareControl.getSquare(point.x,i-1);
			if(square.getPiece()!=null && square.getPiece().getColor() == squareControl.getSelectedSquare().getPiece().getColor() ){
				break;
			}
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor()){
				break;
			}

		}
		for (int i = point.x; i >= 0; i--) {
			square = squareControl.getSquare(i-1,point.y);
			if(square.getPiece()!=null && square.getPiece().getColor() == squareControl.getSelectedSquare().getPiece().getColor() ){
				break;
			}
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor()){
				break;
			}

		}
		for (int i = point.x; i < SquareControl.COL_NUMBER; i++) {
			square = squareControl.getSquare(i+1,point.y);
			if(square.getPiece()!=null && square.getPiece().getColor() == squareControl.getSelectedSquare().getPiece().getColor() ){
				break;
			}
			square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor()){
				break;
			}
		}
	}
	public void unShowMove(SquareControl squareControl){	
		Square square;		
		Point point = squareControl.getSelectedSquare().getPosition();
		
		for (int i = point.y; i < SquareControl.COL_NUMBER; i++) {
			square = squareControl.getSquare(point.x,i);
			squareControl.resetColor(square);
		}		
		for (int i = point.x; i < SquareControl.COL_NUMBER; i++) {
			square = squareControl.getSquare(i,point.y);	
			squareControl.resetColor(square);
		}	
		for (int i = point.y; i >= 0; i--) {
			square = squareControl.getSquare(point.x,i);	
			squareControl.resetColor(square);
		}
		for (int i = point.x; i >= 0; i--) {
			square = squareControl.getSquare(i,point.y);	
			squareControl.resetColor(square);
		}
	}


}
