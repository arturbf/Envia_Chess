package model;


import control.SquareControl;


public abstract class Piece {
	protected Boolean firstMove;
	
	public Boolean getFirstMove() {
		return firstMove;
	}
	public static final String EMPTY_PATH = "";


	private String imagePath;
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Piece() {
		super();
		
	}

	public Piece(String imagePath) {
		super();
		this.firstMove=true;
		this.imagePath = imagePath;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public void removerImagePath(String imagePath) {
		this.imagePath = EMPTY_PATH;
	}

	public boolean haveImagePath() {
		return this.imagePath != EMPTY_PATH;
	}
	public abstract void unShowMove(SquareControl squareControl);
	public abstract void showMove(SquareControl squareControl);
	public boolean move(Square square){
		
		if(square.getColor() == SquareControl.DEFAULT_COLOR_POSSIBLE_SELECTED){
			firstMove=false;
			return true;
		}
		else
			return false;
	}
}