package model;

import java.awt.Color;
import java.awt.Point;

import javax.swing.JOptionPane;

import control.SquareControl;

public class Pawn extends Piece {
	

	public Pawn(String imagePath) {
		super(imagePath);
	
		if(imagePath.compareTo("icon/Yellow P_48x48.png")==0){
			setColor("Yellow");
		}
		else{
			setColor("Black");
		}
	}

	@Override
	public void unShowMove(SquareControl squareControl) {
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		if(squareControl.getSelectedSquare().getPiece().getColor()== "Yellow"){
			square = squareControl.getSquare(point.x-1,point.y);
			squareControl.resetColor(square);
			square = squareControl.getSquare(point.x-2,point.y);
			squareControl.resetColor(square);
			square = squareControl.getSquare(point.x-1,point.y+1);
			squareControl.resetColor(square);
			square = squareControl.getSquare(point.x-1,point.y-1);
			squareControl.resetColor(square);
		}
		if(squareControl.getSelectedSquare().getPiece().getColor()== "Black"){
			square = squareControl.getSquare(point.x+2,point.y);
			squareControl.resetColor(square);
			square = squareControl.getSquare(point.x+1,point.y);
			squareControl.resetColor(square);
			square = squareControl.getSquare(point.x+1,point.y+1);
			squareControl.resetColor(square);
			square = squareControl.getSquare(point.x+1,point.y-1);
			squareControl.resetColor(square);
		}
		
	}

	@Override
	public void showMove(SquareControl squareControl) {
		Point point = squareControl.getSelectedSquare().getPosition();
		Square square;
		if(squareControl.getSelectedSquare().getPiece().getColor()== "Yellow"){
			if(firstMove){
				square = squareControl.getSquare(point.x-1,point.y);
				if(square.getPiece()==null){
					square = squareControl.getSquare(point.x-2,point.y);
					if(square.getPiece()==null)
						square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
				}
			}				
			square = squareControl.getSquare(point.x-1,point.y);
			if(square.getPiece()==null)
				square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			
			square = squareControl.getSquare(point.x-1,point.y+1);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
				square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			square = squareControl.getSquare(point.x-1,point.y-1);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
				square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			
		}
		if(squareControl.getSelectedSquare().getPiece().getColor()== "Black"){
			if(firstMove){
				square = squareControl.getSquare(point.x+1,point.y);
				if(square.getPiece()==null){
				square = squareControl.getSquare(point.x+2,point.y);
				if(square.getPiece()==null)
					square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
				}
			}
			square = squareControl.getSquare(point.x+1,point.y);
			if(square.getPiece()==null)
				square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			
			square = squareControl.getSquare(point.x+1,point.y+1);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
				square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
			square = squareControl.getSquare(point.x+1,point.y-1);
			if(square.getPiece()!=null && square.getPiece().getColor() != squareControl.getSelectedSquare().getPiece().getColor() )
				square.setColor(squareControl.DEFAULT_COLOR_POSSIBLE);
		}		
	}
	
	public void changePiece(Square square){
		square.setPiece(changePawn());		
		
	}
	
	public Piece changePawn(){
		String opcoes[]={"Torre","Cavalo","Bispo","Rainha"};
		int i = JOptionPane.showOptionDialog(null, "Escolha por qual deseja trocar", "Troca",0, JOptionPane.QUESTION_MESSAGE, null, opcoes, null);
		while(i==-1)
			i = JOptionPane.showOptionDialog(null, "Escolha por qual deseja trocar", "Troca",0, JOptionPane.QUESTION_MESSAGE, null, opcoes, null);
		switch(i){
		case 0:
			return new Rook(selectImagePath(i, this.getColor()));
		case 1:
			return new Knight(selectImagePath(i, this.getColor()));
		case 2:
			return new Bishop(selectImagePath(i, this.getColor()));
		case 3:
			return new Queen(selectImagePath(i, this.getColor()));
		}
		return null;
	}
		public String selectImagePath(int i,String cor){
			if(cor.equalsIgnoreCase("Black")){
				switch(i){
				case 0:
					return "icon/Black R_48x48.png";
				case 1:
					return "icon/Black N_48x48.png";
				case 2:
					return "icon/Black B_48x48.png";
				case 3:
					return "icon/Black Q_48x48.png";
				}
			}else{	
				switch(i){
				case 0:
					return "icon/Yellow R_48x48.png";
				case 1:
					return "icon/Yellow N_48x48.png";
				case 2:
					return "icon/Yellow B_48x48.png";
				case 3:
					return "icon/Yellow Q_48x48.png";
				}

			}
			return null;
		}

}