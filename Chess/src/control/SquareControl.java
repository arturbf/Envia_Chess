package control;

import java.awt.Color;
import java.awt.Point;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import view.SquareBoardPanel;


import model.Bishop;
import model.King;
import model.Knight;
import model.Pawn;
import model.Piece;
import model.Queen;
import model.Rook;
import model.Square;
import model.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;
	public static final int  MAX_INDEX = 64;
	public static final int MIN_INDEX = 0;
	

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_POSSIBLE = Color.CYAN;
	public static final Color DEFAULT_COLOR_POSSIBLE_SELECTED = Color.MAGENTA;
	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;			
	private Color colorSelected;
	
//	private int flag;
//	private int flag2 = 0;
//	private int jogada;

	private Square selectedSquare;
	
//	private void message(String message){
//		JOptionPane.showMessageDialog(null, message,"Iniciando o jogo",JOptionPane.INFORMATION_MESSAGE);
//	}
	
	public Square getSelectedSquare() {
		return selectedSquare;
	}

	private ArrayList<Square> squareList;
	

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;

		this.squareList = new ArrayList<Square>();	
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;
		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		if(square.getColor()!= DEFAULT_COLOR_POSSIBLE){	
			square.setColor(this.colorHover);
		}else
			square.setColor(this.DEFAULT_COLOR_POSSIBLE_SELECTED);
	}

	@Override
	public void onSelectEvent(Square square) {
		
		if (haveSelectedCellPanel()) {							
			try {
				if(!this.selectedSquare.equals(square)) {	
					moveContentOfSelectedSquare(square);

				} else {
					unselectSquare(square,false);					
				}
			}
			catch (Execao e){
				e.message("Jogue novamente");
			}
		} else {
			selectSquare(square);
		}
	}

	@Override
	public void onOutEvent(Square square) {
		if(square.getColor()!= DEFAULT_COLOR_POSSIBLE_SELECTED){
			if (this.selectedSquare != square ) {
				resetColor(square);
			} else {
				square.setColor(this.colorSelected);
			}
		}else{
			square.setColor(DEFAULT_COLOR_POSSIBLE);
		}
		
	}

	public Square getSquare(int row, int col) {
		if(row<ROW_NUMBER && row>=0 && col<COL_NUMBER && col>=0 )
			return this.squareList.get((row * COL_NUMBER) + col);
		else
			return new Square();
		
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;
		
		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) throws Execao {
		if (selectedSquare.getPiece().move(square)){
			if(square.getPiece()!=null && square.getPiece().getClass()==King.class){
				if(JOptionPane.showConfirmDialog(null, "Cheque mate\ndeseja reiniciar?","Opcao",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE)==JOptionPane.YES_OPTION){
					zeraQuadrados();
					unselectSquare(square, false);
					//					System.out.println("reiniciou");
				}
			}else{
				square.setPiece(this.selectedSquare.getPiece());
				if(isPawn(square.getPiece(), square.getPosition())){
					square.getPiece().unShowMove(this);
					Pawn auxPawn = (Pawn) square.getPiece();
					auxPawn.changePiece(square);
					
				}

				//this.selectedSquare.getPiece().unShowMove(this);
				//this.selectedSquare.removePiece();
				//this.selectedSquare= EMPTY_SQUARE;//
				unselectSquare(square,true);
			}
		}
		else
			throw new Execao("Movimento inválido");
	}

	public boolean isPawn(Piece piece,Point posicao){
		if(piece.getClass()==Pawn.class && (posicao.getX()==0 || posicao.getX()==7))
			return true;
		else 
			return false;
	}
	
	public void zeraQuadrados(){
		for(Square square: squareList){
			this.resetColor(square);
			square.setPiece(null);
//			unselectSquare(square, isMove);
			resetPiece();
		}
	}
	public void resetPiece(){
			Piece peacePath ;
			for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
				peacePath = new Pawn ("icon/Black P_48x48.png");
			getSquare(1, i).setPiece(peacePath);
			}
			peacePath = new Rook ("icon/Black R_48x48.png");
			getSquare(0, 0).setPiece(peacePath);
			getSquare(0, 7).setPiece(peacePath);

			peacePath = new Knight ("icon/Black N_48x48.png");
			getSquare(0, 1).setPiece(peacePath);
			getSquare(0, 6).setPiece(peacePath);

			peacePath = new Bishop ("icon/Black B_48x48.png");
			getSquare(0, 2).setPiece(peacePath);
			getSquare(0, 5).setPiece(peacePath);

			peacePath = new Queen ("icon/Black Q_48x48.png");
			getSquare(0, 4).setPiece(peacePath);

			peacePath = new King ("icon/Black K_48x48.png");
			getSquare(0, 3).setPiece(peacePath);

			
			for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
				peacePath = new Pawn ("icon/Yellow P_48x48.png");
				getSquare(6, i).setPiece(peacePath);
			}
			
			peacePath = new Rook ("icon/Yellow R_48x48.png");
			getSquare(7, 0).setPiece(peacePath);
			getSquare(7, 7).setPiece(peacePath);

			peacePath = new Knight ("icon/Yellow N_48x48.png");
			getSquare(7, 1).setPiece(peacePath);
			getSquare(7, 6).setPiece(peacePath);

			peacePath = new Bishop ("icon/Yellow B_48x48.png");
			getSquare(7, 2).setPiece(peacePath);
			getSquare(7, 5).setPiece(peacePath);

			peacePath = new Queen ("icon/Yellow Q_48x48.png");
			getSquare(7, 4).setPiece(peacePath);

			peacePath = new King ("icon/Yellow K_48x48.png");				
			getSquare(7, 3).setPiece(peacePath);

		
	}
	private void selectSquare(Square square) {
		if (square.havePiece()) {	
			this.selectedSquare = square;
			if(square.getPiece()!=null){
				square.getPiece().showMove(this);
			}
			//this.selectedSquare.setColor(this.colorSelected);
		}
	}
	
	private void unselectSquare(Square square,boolean isMove) {
		square.getPiece().unShowMove(this);
		resetColor(this.selectedSquare);
		if(isMove)
			this.selectedSquare.removePiece();
		this.selectedSquare = EMPTY_SQUARE;
	}
		
	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	
}
