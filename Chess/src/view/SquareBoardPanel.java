package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Bishop;
import model.King;
import model.Knight;
import model.Pawn;
import model.Queen;
import model.Rook;
import model.Square;
import model.Piece;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		Piece peacePath ;
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			peacePath = new Pawn ("icon/Black P_48x48.png");
			this.squareControl.getSquare(1, i).setPiece(peacePath);
		}
		peacePath = new Rook ("icon/Black R_48x48.png");
		this.squareControl.getSquare(0, 0).setPiece(peacePath);
		this.squareControl.getSquare(0, 7).setPiece(peacePath);

		peacePath = new Knight ("icon/Black N_48x48.png");
		this.squareControl.getSquare(0, 1).setPiece(peacePath);
		this.squareControl.getSquare(0, 6).setPiece(peacePath);

		peacePath = new Bishop ("icon/Black B_48x48.png");
		this.squareControl.getSquare(0, 2).setPiece(peacePath);
		this.squareControl.getSquare(0, 5).setPiece(peacePath);

		peacePath = new Queen ("icon/Black Q_48x48.png");
		this.squareControl.getSquare(0, 4).setPiece(peacePath);

		peacePath = new King ("icon/Black K_48x48.png");
		this.squareControl.getSquare(0, 3).setPiece(peacePath);

		
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			peacePath = new Pawn ("icon/Yellow P_48x48.png");
			this.squareControl.getSquare(6, i).setPiece(peacePath);
		}
		
		peacePath = new Rook ("icon/Yellow R_48x48.png");
		this.squareControl.getSquare(7, 0).setPiece(peacePath);
		this.squareControl.getSquare(7, 7).setPiece(peacePath);

		peacePath = new Knight ("icon/Yellow N_48x48.png");
		this.squareControl.getSquare(7, 1).setPiece(peacePath);
		this.squareControl.getSquare(7, 6).setPiece(peacePath);

		peacePath = new Bishop ("icon/Yellow B_48x48.png");
		this.squareControl.getSquare(7, 2).setPiece(peacePath);
		this.squareControl.getSquare(7, 5).setPiece(peacePath);

		peacePath = new Queen ("icon/Yellow Q_48x48.png");
		this.squareControl.getSquare(7, 4).setPiece(peacePath);

		peacePath = new King ("icon/Yellow K_48x48.png");				
		this.squareControl.getSquare(7, 3).setPiece(peacePath);

	}
}
